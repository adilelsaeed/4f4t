<?php

class FT_Custom_Metaboxes {

	/**
	 * Constructor function.
	 * @access  public
	 * @since   1.0.0
	 * @return  void
	 */
	public function __construct() {
		add_action( 'cmb2_admin_init', array( $this, 'contacts_metaboxes' ) );
	}


	public function contacts_metaboxes() {
		$prefix = 'ft_';

		$contact_cmb = new_cmb2_box( array(
			'id'            => $prefix . 'contact_metabox',
			'title'         => esc_html__( 'Contact Details', '4f4t-plugin' ),
			'object_types'  => array( 'contact' ), // Post type
		) );

		$contact_cmb->add_field( array(
			'name'    => esc_html__( 'Phone', '4f4t-plugin' ),
			'id'      => $prefix . 'contact_phone',
			'type'    => 'text_medium'
		) );

		$contact_cmb->add_field( array(
			'name'    => esc_html__( 'Address', '4f4t-plugin' ),
			'id'      => $prefix . 'contact_address',
			'type'    => 'textarea_small'
		) );
	}

} // End Class