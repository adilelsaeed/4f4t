<?php

class FT_Custom_Post_Types {

	/**
	 * Constructor function.
	 * @access  public
	 * @since   1.0.0
	 * @return  void
	 */
	public function __construct() {
		add_action( 'init', array( $this, 'custom_post_types' ) );
		add_action( 'init', array( $this, 'custom_taxonomies' ) );
	}


	// Register Custom Post Types
	public function custom_post_types() {
		
		/**
		 * Post Type: Contacts.
		 */

		$labels = array(
			"name" => __( "Contacts", "4f4t-plugin" ),
			"singular_name" => __( "Contact", "4f4t-plugin" ),
			"menu_name" => __( "Contacts", "4f4t-plugin" ),
			"all_items" => __( "All Contacts", "4f4t-plugin" ),
			"add_new" => __( "Add New", "4f4t-plugin" ),
			"add_new_item" => __( "Add New Contact", "4f4t-plugin" ),
			"edit_item" => __( "Edit Contact", "4f4t-plugin" ),
			"new_item" => __( "New Contact", "4f4t-plugin" ),
			"view_item" => __( "View Contact", "4f4t-plugin" ),
			"view_items" => __( "View Contacts", "4f4t-plugin" ),
			"search_items" => __( "Search Contacts", "4f4t-plugin" ),
			"not_found" => __( "No Contacts found", "4f4t-plugin" ),
			"not_found_in_trash" => __( "No Contacts found in Trash", "4f4t-plugin" ),
			"archives" => __( "Contacts Archives", "4f4t-plugin" ),
		);

		$args = array(
			"label" => __( "Contacts", "4f4t-plugin" ),
			"labels" => $labels,
			"description" => "",
			"public" => true,
			"publicly_queryable" => true,
			"show_ui" => true,
			"show_in_rest" => false,
			"rest_base" => "",
			"has_archive" => false,
			"show_in_menu" => true,
			"exclude_from_search" => false,
			"capability_type" => "post",
			"map_meta_cap" => true,
			"hierarchical" => false,
			"rewrite" => array( "slug" => "contact", "with_front" => true ),
			"query_var" => true,
			"menu_icon" => "dashicons-id",
			"supports" => array( "title", "editor", "thumbnail" ),
			"taxonomies" => array( "group" ),
		);

		register_post_type( "contact", $args );

	}


	// Register Custom Taxonomies
	public function custom_taxonomies() {

		/**
		 * Taxonomy: Groups.
		 */

		$labels = array(
			"name" => __( "Groups", "4f4t-plugin" ),
			"singular_name" => __( "Group", "4f4t-plugin" ),
			"menu_name" => __( "Groups", "4f4t-plugin" ),
			"all_items" => __( "All Groups", "4f4t-plugin" ),
			"edit_item" => __( "Edit Group", "4f4t-plugin" ),
			"view_item" => __( "View Group", "4f4t-plugin" ),
			"update_item" => __( "Update Group Name", "4f4t-plugin" ),
			"add_new_item" => __( "Add New Group", "4f4t-plugin" ),
			"new_item_name" => __( "New Group Name", "4f4t-plugin" ),
			"parent_item" => __( "Parent Group", "4f4t-plugin" ),
			"parent_item_colon" => __( "Parent Group:", "4f4t-plugin" ),
			"search_items" => __( "Search Groups", "4f4t-plugin" ),
			"popular_items" => __( "Popular Groups", "4f4t-plugin" ),
			"add_or_remove_items" => __( "Add or remove Groups", "4f4t-plugin" ),
			"choose_from_most_used" => __( "Choose From Most Used Groups", "4f4t-plugin" ),
			"not_found" => __( "No Groups found", "4f4t-plugin" ),
			"no_terms" => __( "No Groups", "4f4t-plugin" ),
		);

		$args = array(
			"label" => __( "Groups", "4f4t-plugin" ),
			"labels" => $labels,
			"public" => true,
			"hierarchical" => true,
			"label" => "Groups",
			"show_ui" => true,
			"show_in_menu" => true,
			"show_in_nav_menus" => true,
			"query_var" => true,
			"rewrite" => array( 'slug' => 'group', 'with_front' => true, ),
			"show_admin_column" => false,
			"show_in_rest" => false,
			"rest_base" => "",
			"show_in_quick_edit" => false,
		);
		register_taxonomy( "group", array( "contact" ), $args );

	}


} // End Class



