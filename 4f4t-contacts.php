<?php
/**
 * Plugin Name:			4F4T Contacts Plugin
 * Plugin URI:			http://4f4t.com/
 * Description:			Add Contacts Custom Post Type and Its details.
 * Version:				0.1.0
 * Author:				Adil Elsaeed
 * Author URI:			http://www.adilelsaeed.com/
 * Requires at least:	4.0.0
 * Tested up to:		4.7.4
 *
 * Text Domain: 4f4t-plugin
 * Domain Path: /languages/
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Returns the main instance of FT_Plugin to prevent the need to use globals.
 *
 * @since  1.0.0
 * @return object FT_Plugin
 */
function FT_Plugin() {
	return FT_Plugin::instance();
} // End FT_Plugin()

FT_Plugin();

/**
 * Main FT_Plugin Class
 *
 * @class FT_Plugin
 * @version	1.0.0
 * @since 1.0.0
 * @package	FT_Plugin
 */
final class FT_Plugin {
	/**
	 * FT_Plugin The single instance of FT_Plugin.
	 * @var 	object
	 * @access  private
	 * @since 	1.0.0
	 */
	private static $_instance = null;

	/**
	 * The token.
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $token;

	/**
	 * The version number.
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $version;

	// Admin - Start
	/**
	 * The admin object.
	 * @var     object
	 * @access  public
	 * @since   1.0.0
	 */
	public $admin;

	/**
	 * Constructor function.
	 * @access  public
	 * @since   1.0.0
	 * @return  void
	 */
	public function __construct( $widget_areas = array() ) {
		$this->token 			= '4f4t-plugin';
		$this->plugin_url 		= plugin_dir_url( __FILE__ );
		$this->plugin_path 		= plugin_dir_path( __FILE__ );
		$this->version 			= '0.1.0';

		define( 'FT_PATH', $this->plugin_path );
		define( 'FT_VERSION', $this->version );
		// Include files
        $this->includes();

        // Instantiate classes
        $this->instantiate();

        // Initialize the action hooks
        $this->init_actions();
	}

	/**
    * Init the action/filter hooks
    *
    * @return void
    */
	public function init_actions() {
		register_activation_hook( __FILE__, array( $this, 'install' ) );

		add_action( 'init', array( $this, 'ft_load_plugin_textdomain' ) );
	}

	/**
     * Instantiate the classes
     *
     * @return void
     */
    public function instantiate(){
        new FT_Custom_Post_Types();
        new FT_Custom_Metaboxes();
    }


    /**
     * Include the files
     *
     * @return void
     */
    public function includes() {
    	// CMB2 Libararay, for life website we should use WordPress plugin repository version but this is just for dev task
    	require_once( FT_PATH .'/lib/cmb2/init.php' );
    	// custom post types inits
        require_once( FT_PATH .'/inc/cpt.php' );
		// custom metaboxes inits
		require_once( FT_PATH .'/inc/cmb.php' );

        if ( is_admin() ) {
        	// no admin includes
        }

    }

	/**
	 * Main FT_Plugin Instance
	 *
	 * Ensures only one instance of FT_Plugin is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 * @static
	 * @see FT_Plugin()
	 * @return Main FT_Plugin instance
	 */
	public static function instance() {
		if ( is_null( self::$_instance ) )
			self::$_instance = new self();
		return self::$_instance;
	} // End instance()

	/**
	 * Load the localisation file.
	 * @access  public
	 * @since   1.0.0
	 * @return  void
	 */
	public function ft_load_plugin_textdomain() {
		load_plugin_textdomain( '4f4t-plugin', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
	}

	/**
	 * Cloning is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __clone() {
		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?' ), '1.0.0' );
	}

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __wakeup() {
		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?' ), '1.0.0' );
	}

	/**
	 * Installation.
	 * Runs on activation. Logs the version number and assigns a notice message to a WordPress option.
	 * @access  public
	 * @since   1.0.0
	 * @return  void
	 */
	public function install() {
		$this->_log_version_number();
	}

	/**
	 * Log the plugin version number.
	 * @access  private
	 * @since   1.0.0
	 * @return  void
	 */
	private function _log_version_number() {
		// Log the version number.
		update_option( $this->token . '-version', $this->version );
	}


} // End Class
